from flask import Flask, jsonify, request
from . import model

app = Flask(__name__)


@app.route('/<uname>')
def hello_world(uname):
    user = model.User.query.filter_by(username=uname).first()
    if user is None:
        return "User not found", 404
    return(jsonify({'message': 'Hello %s!' % uname,
                    'email': user.email,
                    'id': user.id}))


def stock_limit_reached():
    total = sum([i.stock for i in model.InventoryItem.query.all()])
    return total >= 500


@app.route('/stock/add/<int:id>')
def add_stock(id):
    if stock_limit_reached():
        return 'Inventory Full', 400
    item = model.InventoryItem.query.get(id)
    amount = int(request.args.get('amount'))
    item.stock += amount
    if not stock_limit_reached():
        model.db.session.commit()
        return 'Stock added', 201
    else:
        model.db.session.rollback()
        return 'Inventory Full', 400
