from flask import Flask, jsonify, request
from . import model
from .calculate import add, sqrt, divide

app = Flask(__name__)


@app.route('/<uname>')
def hello_world(uname):
    user = model.User.query.filter_by(username=uname).first()
    if user is None:
        return "User not found", 404
    return(jsonify({'message': 'Hello %s!' % uname,
                    'email': user.email,
                    'id': user.id}))


@app.route('/math/add')
def do_add():
    """
    Add two arguments provided in the querystring.
    It is intentional that this code doesn't function
    as expected yet.
    """
    a = request.args.get('a')
    b = request.args.get('b')
    try:
        res = add(a, b)
    except Exception as e:
        return jsonify({'message': str(e), 'result': None})
    return jsonify({'message': 'successful', 'result': res})


@app.route('/math/sqrt')
def do_sqrt():
    """
    Take the square root of an argument provided in the querystring.
    It is intentional that this code doesn't function
    as"
    """
    num = request.args.get('num')
    try:
        res = sqrt(num)
    except Exception as e:
        return jsonify({'message': str(e), 'result': None})
    return jsonify({'message': 'successful', 'result': res})


@app.route('/math/divide')
def do_divide():
    """
    Add two arguments provided in the querystring.
    It is intentional that this code doesn't function
    as expected yet.
    """
    a = request.args.get('a')
    b = request.args.get('b')
    try:
        res = divide(a, b)
    except Exception as e:
        return jsonify({'message': str(e), 'result': None})
    return jsonify({'message': 'successful', 'result': res})
