import math


def checknumber(num):
    """
    Check if the argument num is numeric.
    """
    if isinstance(num, str):
        return float(num)
    elif isinstance(num, int) or isinstance(num, float):
        return num
    else:
        raise TypeError("Argument is not numeric")


def add(a, b):
    # Add two numbers and return the result
    return checknumber(a)+checknumber(b)


def sqrt(num):
    # Return the square root of a number
    num = checknumber(num)
    if num < 0:
        raise ValueError("Square root of a number <0 is not allowed.")
    return math.sqrt(checknumber(num))


def divide(a, b):
    # Return the division of arguments a and b
    a = checknumber(a)
    b = checknumber(b)
    if b == 0:
        raise ValueError("Division by 0 is not allowed.")
    return a/b
