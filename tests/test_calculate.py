from unittest import TestCase
from my_tdd_app import calculate


class test_calculate(TestCase):
    def test_add(self):
        """
        Test the add function. It should add the two arguments together.
        """
        result = calculate.add(5, 3)
        self.assertEqual(result, 8)
        result = calculate.add(5, "3")
        self.assertEqual(result, 8)
        result = calculate.add("5.0", 3)
        self.assertEqual(result, 8)

    def test_add_string(self):
        """
        Test adding strings together.
        """
        with self.assertRaises(ValueError):
            calculate.add("aa", 0)
        with self.assertRaises(ValueError):
            calculate.add(5, "b")

    def test_sqrt(self):
        """
        Test the sqrt function.
        """
        result = calculate.sqrt(9)
        self.assertEqual(result, 3)
        result = calculate.sqrt("16")
        self.assertEqual(result, 4)

    def test_sqrt_invalid_values(self):
        """
        Test the sqrt function.
        """
        with self.assertRaises(ValueError):
            calculate.sqrt(-2)
        with self.assertRaises(TypeError):
            calculate.sqrt(print)

    def test_divide(self):
        """
        Test the divide function.
        """
        self.assertEqual(calculate.divide(12, 4), 3)
        self.assertEqual(calculate.divide("12", "4"), 3)

    def test_divide_invalid_values(self):
        """
        Test invalid values for the divide function.
        """
        with self.assertRaises(ValueError):
            calculate.divide(25, 0)
        with self.assertRaises(TypeError):
            calculate.divide(25, print)
        with self.assertRaises(ValueError):
            calculate.divide("bla", 0)
