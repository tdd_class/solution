from unittest import TestCase
from my_tdd_app.calc_app import app, model
import json


class MathAppTest(TestCase):
    """
    Test the math app.
    """

    def setUp(self):
        """
        Setup the app and data model.
        """
        self.app = app
        self.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        self.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'

        # This is some magic to get a Flask context
        ctx = self.app.app_context()
        ctx.push()

        # Setup the database and model
        model.db.init_app(app)
        model.db.create_all()
        model.db.session.commit()

        # Setup the client that we will use in the tests
        self.client = self.app.test_client()

    def test_hello_world(self):
        """
        Test the hello world function.
        """
        # Add a test user first.
        testuser = model.User(username='some_test_user',
                              email='test@nightowl.foundationu.com')
        model.db.session.add(testuser)
        model.db.session.commit()

        # Now send an HTTP request to the application
        resp = self.client.get('/some_test_user')

        # Load the JSON response into a python dictionary
        resp_data = json.loads(resp.get_data(as_text=True))

        # Now test the resulting data
        self.assertEqual(resp_data['email'], testuser.email)
        self.assertEqual(resp_data['id'], testuser.id)
        self.assertEqual(resp_data['message'], 'Hello %s!' % testuser.username)

    def test_add(self):
        """
        Test the /math/add url.
        """

        # Send an HTTP request to the application
        resp = self.client.get('/math/add?a=2&b=3')

        # Load the JSON response into a python dictionary
        resp_data = json.loads(resp.get_data(as_text=True))

        # Now test the resulting data
        self.assertEqual(resp_data['result'], 5)

        # Now test invalid values
        resp = self.client.get('/math/add?a=5&b=loreto')
        resp_data = json.loads(resp.get_data(as_text=True))
        self.assertEqual(resp_data['message'], 
                         "could not convert string to float: 'loreto'")

    def test_divide(self):
        """
        Test the /math/divide url.
        """

        # Send an HTTP request to the application
        resp = self.client.get('/math/divide?a=2&b=3')

        # Load the JSON response into a python dictionary
        resp_data = json.loads(resp.get_data(as_text=True))

        # Now test the resulting data
        self.assertAlmostEqual(resp_data['result'], 0.66667, 5)

        # Test with string values
        resp = self.client.get('/math/divide?a=dolf&b=3')
        resp_data = json.loads(resp.get_data(as_text=True))
        self.assertEqual(resp_data['message'],
                         "could not convert string to float: 'dolf'")

        # Test division by 0
        resp = self.client.get('/math/divide?a=2&b=0')
        resp_data = json.loads(resp.get_data(as_text=True))
        self.assertEqual(resp_data['message'], "Division by 0 is not allowed.")

    def test_sqrt(self):
        """
        Test the /math/divide url.
        """

        # Send an HTTP request to the application
        resp = self.client.get('/math/sqrt?num=9')

        # Load the JSON response into a python dictionary
        resp_data = json.loads(resp.get_data(as_text=True))

        # Now test the resulting data
        self.assertEqual(resp_data['result'], 3)

        # Test with string values
        resp = self.client.get('/math/sqrt?num=dolf')
        resp_data = json.loads(resp.get_data(as_text=True))
        self.assertEqual(resp_data['message'],
                         "could not convert string to float: 'dolf'")

        # Test with value<0
        resp = self.client.get('/math/sqrt?num=-1')
        resp_data = json.loads(resp.get_data(as_text=True))
        self.assertEqual(resp_data['message'],
                         "Square root of a number <0 is not allowed.")
