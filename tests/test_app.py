from unittest import TestCase
from my_tdd_app.app import app, model


class MyFirstTest(TestCase):
    """
    Simple Test Class to test the Flask application.
    """

    def setUp(self):
        """
        setUp is called before every test function.
        It is used to do some basic setup stuff before you run the
        actual test.
        """
        self.app = app
        self.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        self.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'

        # This is some magic to get a Flask context
        ctx = self.app.app_context()
        ctx.push()

        """
        We also need to setup the database and create all tables.
        In this case because we don't configure anything SQLAlchemy will use
        an SQLite in-memory database.
        """
        model.db.init_app(app)
        model.db.create_all()
        model.db.session.commit()

        # Setup the client that we will use in the tests
        self.client = self.app.test_client()

        # Add a few test items
        for name in ['sex, drugs, rock&roll']:
            item = model.InventoryItem(name=name)
            model.db.session.add(item)
        model.db.session.commit()

    def test_stock_add_limit(self):
        """
        Test adding stock and reaching the limit (500).
        """

        response = self.client.get('/stock/add/1?amount=10')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.get_data(as_text=True), 'Stock added')

        response = self.client.get('/stock/add/1?amount=500')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.get_data(as_text=True), 'Inventory Full')

        response = self.client.get('/stock/add/1?amount=489')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.get_data(as_text=True), 'Stock added')

        response = self.client.get('/stock/add/1?amount=1')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.get_data(as_text=True), 'Inventory Full')
