import math
from unittest import TestCase


def sqrt(num):
    if num < 0:
        return 'Square root of a negative number not allowed!'
    return math.sqrt(num)


def add(a, b):
    return a+b


class test_math(TestCase):
    def test_add(self):
        result = add(5, 2)
        self.assertEqual(result, 7)

    def test_sqrt(self):
        result = sqrt(9)
        self.assertEqual(result, 3)

    def test_sqrt_negative(self):
        result = sqrt(-1)
        self.assertEqual(result,
                         'Square root of a negative number not allowed!')
