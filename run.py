from flask_ini import FlaskIni
import sys
if len(sys.argv) > 1 and sys.argv[1] == 'calculator':
    appname = 'calculator'
    from my_tdd_app.calc_app import app, model
else:
    appname = 'inventory'
    from my_tdd_app.app import app, model


if __name__ == '__main__':
    app.iniconfig = FlaskIni()
    with app.app_context():
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        app.iniconfig.read('instance/config.ini')
        model.db.init_app(app)
        model.db.create_all()
        if appname == 'calculator':
            u = model.User(username='testuser', email='test@test.com')
            model.db.session.add(u)
            model.db.session.commit()
        elif appname == 'inventory':
            for name in ['sex', 'drugs', 'rock&roll']:
                i = model.InventoryItem(name=name)
                model.db.session.add(i)
            model.db.session.commit()
    app.run()
